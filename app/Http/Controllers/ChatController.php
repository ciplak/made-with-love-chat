<?php

namespace App\Http\Controllers;

use App\Chat;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ChatController extends Controller
{

    /**
     * Save Message
     * @author Safak
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function save()
    {
        $chat = Chat::create([
            'from' => Auth::id(),
            'to' => request('to'),
            'message' => request('message'),
        ]);

        $item = new \stdClass();
        $item->message = request('message');
        $item->date = Carbon::parse($chat->created_at)->format('H:i');

        return view('chat_line', compact('item'));
    }

    /**
     * Get Messages
     *
     * @param null $to
     * @author Safak
     * @return \Illuminate\Http\JsonResponse
     */
    public function get()
    {
        $from = Auth::id();
        $to = request('to');

        $messages = Chat::where('from', $to)
            ->where('to', $from)
            ->orWhere('from', $from)
            ->where('to', $to)->get();

        return view('old_messages', compact('messages'));
    }
}
