@foreach($messages as $message)
    <div class="customContainer darker">
        <p>{{$message->message}}</p>
        <span class="time-left">{{\Carbon\Carbon::parse($message->created_at)->format('H:i')}}</span>
    </div>
@endforeach
