@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row col-md-12 col-md-offset-2 custyle">
            <table class="table table-striped custab">
                <thead>
                <h1>Users</h1>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th class="text-center">Action</th>
                </tr>
                </thead>
                @foreach($users as $user)
                    <tr>
                        <td>{{$user->id}}</td>
                        <td>{{$user->name}}</td>
                        <td class="text-center">
                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                    data-target="#exampleModalCenter{{$user->id}}" onclick="getMessages({{$user->id}})">
                                Chat
                            </button>
                        </td>
                    </tr>
                    <!-- Modal -->
                    <div class="modal fade" id="exampleModalCenter{{$user->id}}" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h2>Chat Messages with {{$user->name}}</h2>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div id="chatBox-{{$user->id}}">
                                        <div class="customContainer">
                                            <p>Hello. How are you today?</p>
                                            <span class="time-right">11:00</span>
                                        </div>

                                        <div class="customContainer darker">
                                            <p>Hey! I'm fine. Thanks for asking!</p>
                                            <span class="time-left">11:01</span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Message</label>
                                        <textarea class="form-control" id="message-{{$user->id}}" rows="2"
                                                  required></textarea>
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary" onclick="sendMessage({{$user->id}})">
                                        Send
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </table>
        </div>
    </div>
@endsection

